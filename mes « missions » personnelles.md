---
title: "Mes « missions » personnelles"
order: 1
in_menu: true
---
Aujourd'hui retraité, je m'intéresse plus particulièrement :


## Le zéro déchet

- Je suis membre de Zéro Déchet Troyes

- Du Réseau Compost Citoyen Grand Est


## Le logiciel libre et open source

- Me retrouver sur https://framapiaf.org/@jose_relland

- Microentrepreneur, je peux intervenir dans des structures professionnelles en prestataire pour de la formation sur logiciel libre. Exemple : LibreOffice


## La réduction des gaz à effet de serre

- Actionnaire et évaluateur de Team For The Planet

- Référent de l'Aube de l'association Nos Vies Bas Carbone, initiateur de l'atelier Inventons nous vies bas carbone.


Mise à jour : 11/02/2024 