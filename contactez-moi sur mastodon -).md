---
title: "Contactez-moi sur Mastodon :)"
order: 1
in_menu: true
---
Vous pouvez me laisser un message sur le site de microblogging Mastodon (FEDIVERSE), l'équivalent de X/Twitter, mais en bien mieux. :)

Pour me contacter : [@jose_relland@framapiaf.org ](https://framapiaf.org)

---
Si, vous aussi, vous voulez avoir un compte Mastodon (FEDIVERSE), visiter :

* Pour en savoir plus et pour choisir l'instance (le site) la plus adaptée pour vous : [Rejoindre Mastodon, le réseau social qui n'est pas à vendre !](https://joinmastodon.org/fr)

* [Exemple d'instance qui peut vous accueillir](https://piaille.fr/explore) 