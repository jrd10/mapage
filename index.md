---
title: "Ma page"
order: 0
in_menu: true
---
## C'est ma page jrd10

Une page pour faire un blog simple avec Scribouilli.

Pour l'heure, je teste Scribouilli.


Pour en savoir plus sur Scribouilli et accéder à l'atelier :
[Le site de Scribouilli](https://scribouilli.org/)

Votre site sera sous la forme d'un site sur GitLab. Retrouver le projet sous GitLab : [Le site mapage de jrd10](https://gitlab.com/jrd10/mapage) 


Affaire à suivre, amicalement, José

Le 11/02/2024 

Les minisites de Scribouilli sont édités avec du Markdown. Apprenez Markdown avec [le Guide Markdown](https://flus.fr/markdown) 